
import numpy as np

# Класс – Учебная группа (номер, староста, курс, количество студентов в каждой
# подгруппе.
class StudyGroup:
    number = 200
    headman = 'Ivan'
    course = 1
    __numberOfStudents = 5

    def __init__(self, number, headman, course, numberOfStudents):
        self.number = number
        self.headman = headman
        self.course = course
        self.__numberOfStudents = numberOfStudents

    def getInfoStatic():
        print(f'Number: {StudyGroup.number}')
        print(f'Headman: {StudyGroup.headman}')
        print(f'Course: {StudyGroup.course}')
        print(f'NumberOfStudents: {StudyGroup.__numberOfStudents}')

    def getInfo(self):
        print(f'Number: {self.number}')
        print(f'Headman: {self.headman}')
        print(f'Course: {self.course}')
        print(f'NumberOfStudents: {self.__numberOfStudents}')
        print(f'srBall: {self.srBall}')

    def addBall(self):
        self.__balls = []

        for count in range(self._StudyGroup__numberOfStudents):
            print(f'input ball: ')
            ball = int(input())
            self.__balls.append(ball)

        self.srBall = np.sum(self.__balls)


sg = StudyGroup(1, 'Gena', 2, 4)
sg.addBall()
sg.getInfo()